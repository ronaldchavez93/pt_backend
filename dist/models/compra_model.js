"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
let CompraSchema;
let validStatus = {
    values: ['PENDIENTE', 'CONFIMADO', 'RECHAZADO'],
    message: '{VALUE} no es un estatus valido'
};
CompraSchema = new mongoose_1.Schema({
    cliente: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'clientes'
    },
    token: {
        type: String,
        required: [true, 'Token requerido'],
        trim: true
    },
    monto: {
        type: Number,
        required: [true, 'monto es requerido'],
        default: 0
    },
    estatus: {
        type: String,
        trim: true,
        default: 'PENDIENTE',
        enum: validStatus
    },
    fecha: {
        type: Date,
        default: Date.now
    },
});
module.exports = mongoose_1.model('compras', CompraSchema, 'compras');
