"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
let ClienteSchema;
let email_math = [/^(?:[^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*|"[^\n"]+")@(?:[^<>()[\].,;:\s@"]+\.)+[^<>()[\]\.,;:\s@"]{2,63}$/i,
    'Formato de email incorrecto'
];
ClienteSchema = new mongoose_1.Schema({
    documento: {
        type: String,
        maxlength: [15, 'Maximo 15 caracteres'],
        minlength: [4, 'Minimo 4 caracteres'],
        unique: [true, 'Documento duplicado'],
        trim: true
    },
    nombres: {
        type: String,
        maxlength: [30, 'Maximo 30 caracteres'],
        minlength: [2, 'Minimo 2 caracteres'],
        trim: true
    },
    email: {
        type: String,
        required: [true, 'Email requerido'],
        maxlength: [50, 'Maximo 50 caracteres'],
        minlength: [10, 'Minimo 10 caracteres'],
        index: true,
        unique: [true, 'Email duplicado'],
        match: email_math,
        trim: true
    },
    celular: {
        type: String,
        maxlength: [15, 'Maximo 15 caracteres'],
        minlength: [5, 'Minimo 5 caracteres'],
        unique: [true, 'Celular duplicado'],
        trim: true
    },
    clave: {
        type: String,
        required: [true, 'Clave requerida'],
        maxlength: [100, 'Maximo 100 caracteres'],
        minlength: [8, 'Minimo 10 caracteres'],
        trim: true
    }
});
module.exports = mongoose_1.model('clientes', ClienteSchema, 'clientes');
