"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
let BilleteraSchema;
BilleteraSchema = new mongoose_1.Schema({
    saldo: {
        type: Number,
        required: [true, 'saldo es requerido'],
        default: 0
    },
    cliente: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'clientes'
    }
});
module.exports = mongoose_1.model('billeteras', BilleteraSchema, 'billeteras');
