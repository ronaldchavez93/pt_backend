"use strict";
module.exports = {
    port: process.env.PORT || 3001,
    mongoDb: process.env.MONGO_URI || 'mongodb://localhost:27017/wallet'
};
