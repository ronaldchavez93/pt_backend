"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const mongoose_1 = __importDefault(require("mongoose"));
const bodyParser = __importStar(require("body-parser"));
const { mongoDb } = require('./config/config');
/* ROUTERS */
const cliente_router_1 = __importDefault(require("../routers/cliente_router"));
const billetera_router_1 = __importDefault(require("../routers/billetera_router"));
/* FIN */
/* SWAGGER - DOCUMENTACIÓN DE LAS API */
const swagger_ui_express_1 = __importDefault(require("swagger-ui-express"));
const swaggerDocument = __importStar(require("./swagger.json"));
/* FIN */
const cors = require("cors");
class Server {
    constructor(port) {
        this.port = port;
        this.app = express();
        this.config();
        this.setupDb();
        this.app.use(cliente_router_1.default);
        this.app.use(billetera_router_1.default);
    }
    static init(port) {
        return new Server(port);
    }
    setupDb() {
        mongoose_1.default.connect(mongoDb, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true }, (err) => {
            if (err)
                throw err;
            console.log('base de datos ONLINE');
        });
    }
    config() {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(cors(this.configCors()));
        this.app.use('/swagger', swagger_ui_express_1.default.serve, swagger_ui_express_1.default.setup(swaggerDocument));
    }
    configCors() {
        let originsWhitelist = [
            'http://localhost:3000'
        ];
        let corsOptions = {
            origin: function (origin, callback) {
                let isWhitelisted = originsWhitelist.indexOf(origin) !== -1;
                callback(null, isWhitelisted);
            },
            credentials: true
        };
        return corsOptions;
    }
    start(callback) {
        const server = this.app.listen(this.port, callback);
    }
}
exports.default = Server;
