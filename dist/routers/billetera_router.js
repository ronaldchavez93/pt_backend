"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const router = express_1.Router();
const randtoken = require('rand-token');
const { verificarToken } = require('../middlewares/autentication');
const Cliente = require('../models/cliente_model');
const Billetera = require('../models/billetera_model');
const Compra = require('../models/compra_model');
const config_email_1 = __importDefault(require("../config/config_email"));
const configEmail = new config_email_1.default();
router.get('/api/billetera/:documento/:celular', verificarToken, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let params = req.params;
    let clienteDB = null;
    let billeteraDB = null;
    try {
        clienteDB = yield Cliente.findOne({ documento: params.documento, celular: params.celular }).exec();
        if (clienteDB == null) {
            return res.status(400).json({
                ok: false,
                title: 'notificación',
                message: 'Error: cliente no existe',
                type: 'danger'
            });
        }
        billeteraDB = yield Billetera.findOne({ cliente: clienteDB._id }).exec();
        return res.status(200).json({
            ok: true,
            title: 'notificación',
            message: 'operación realizada con éxito',
            billetera: billeteraDB.saldo,
            type: 'success'
        });
    }
    catch (err) {
        return res.status(500).json({
            ok: false,
            title: 'notificación',
            message: err.message,
            type: 'danger'
        });
    }
}));
router.put('/api/billetera/recargar', verificarToken, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let body = req.body;
    let clienteDB = null;
    let billeteraDB = null;
    try {
        clienteDB = yield Cliente.findOne({ documento: body.documento, celular: body.celular }).exec();
        if (clienteDB == null) {
            return res.status(400).json({
                ok: false,
                title: 'notificación',
                message: 'Error: cliente no existe',
                type: 'danger'
            });
        }
        billeteraDB = yield Billetera.findOne({ cliente: clienteDB._id }, 'saldo').exec();
        let newSaldo = parseFloat(billeteraDB.saldo) + parseFloat(body.saldo);
        yield Billetera.updateOne({ cliente: clienteDB._id }, { $set: { saldo: newSaldo } });
        return res.status(200).json({
            ok: true,
            title: 'notificación',
            message: 'operación realizada con éxito',
            type: 'success'
        });
    }
    catch (err) {
        return res.status(500).json({
            ok: false,
            title: 'notificación',
            message: err.message,
            type: 'danger'
        });
    }
}));
router.post('/api/billetera/pagar', verificarToken, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let body = req.body;
    let clienteDB = null;
    let billeteraDB = null;
    let compraDB = null;
    try {
        let token = randtoken.generate(6);
        clienteDB = yield Cliente.findOne({ documento: body.documento, celular: body.celular }).exec();
        if (clienteDB == null) {
            return res.status(400).json({
                ok: false,
                title: 'notificación',
                message: 'Error: cliente no existe',
                type: 'danger'
            });
        }
        billeteraDB = yield Billetera.findOne({ cliente: clienteDB._id }).exec();
        if (billeteraDB == null) {
            return res.status(400).json({
                ok: false,
                title: 'notificación',
                message: 'Error: billetera no existe',
                type: 'danger'
            });
        }
        if (parseFloat(body.monto) > parseFloat(billeteraDB.saldo)) {
            return res.status(400).json({
                ok: false,
                title: 'notificación',
                message: 'Error: El monto de la compra es mayor al saldo disponible en la billetera, por favor recarge',
                type: 'danger'
            });
        }
        /* SE REGISTRA LA COMPRA */
        let newCompra = {
            cliente: clienteDB._id,
            token: token,
            monto: body.monto
        };
        compraDB = yield new Compra(newCompra).save();
        /* FIN */
        /* SE ENVIA EMAIL CON TOKEN PARA CONFIRMAR LA COMPRA */
        let data = {
            token: token
        };
        configEmail.confirmarCompra(clienteDB.email, data);
        /* FIN */
        return res.status(200).json({
            ok: true,
            title: 'notificación',
            message: 'operación realizada con éxito - se ha enviado un email con el token de confirmación',
            compra: compraDB,
            type: 'success'
        });
    }
    catch (err) {
        return res.status(500).json({
            ok: false,
            title: 'notificación',
            message: err.message,
            type: 'danger'
        });
    }
}));
router.put('/api/billetera/confirmar-pago', verificarToken, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let body = req.body;
    let clienteDB = null;
    let billeteraDB = null;
    let compraDB = null;
    try {
        clienteDB = yield Cliente.findOne({ documento: body.documento, celular: body.celular }).exec();
        if (clienteDB == null) {
            return res.status(400).json({
                ok: false,
                title: 'notificación',
                message: 'Error: cliente no existe',
                type: 'danger'
            });
        }
        billeteraDB = yield Billetera.findOne({ cliente: clienteDB._id }).exec();
        if (billeteraDB == null) {
            return res.status(400).json({
                ok: false,
                title: 'notificación',
                message: 'Error: billetera no existe',
                type: 'danger'
            });
        }
        compraDB = yield Compra.findOne({ _id: body.compra, token: body.token }).exec();
        if (compraDB == null) {
            return res.status(400).json({
                ok: false,
                title: 'notificación',
                message: 'Error: token de confirmación de compra invalido',
                type: 'danger'
            });
        }
        if (parseFloat(compraDB.monto) > parseFloat(billeteraDB.saldo)) {
            /* SE ACTUALIZA EL ESTATUS DE LA COMPRA */
            yield Compra.updateOne({ _id: body.compra }, { $set: { estatus: 'RECHAZADO' } });
            /* FIN */
            return res.status(400).json({
                ok: false,
                title: 'notificación',
                message: 'Error: El monto de la compra es mayor al saldo disponible en la billetera, por favor recarge',
                type: 'danger'
            });
        }
        /* SE DESCUENTA EL MONTO DE LA COMPRA EN LA BILLETERA */
        let newSaldo = parseFloat(billeteraDB.saldo) - parseFloat(compraDB.monto);
        yield Billetera.updateOne({ cliente: clienteDB._id }, { $set: { saldo: newSaldo } });
        /* FIN */
        /* SE ACTUALIZA EL ESTATUS DE LA COMPRA */
        yield Compra.updateOne({ _id: body.compra }, { $set: { estatus: 'CONFIMADO' } });
        /* FIN */
        return res.status(200).json({
            ok: true,
            title: 'notificación',
            message: 'operación realizada con éxito',
            saldo: newSaldo,
            type: 'success'
        });
    }
    catch (err) {
        return res.status(500).json({
            ok: false,
            title: 'notificación',
            message: err.message,
            type: 'danger'
        });
    }
}));
exports.default = router;
