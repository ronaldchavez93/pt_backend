"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const nodemailer = require("nodemailer");
const smtpTransport = require('nodemailer-smtp-transport');
const handlebars = require('handlebars');
const fs = require('fs');
const path = require("path");
class ConfigEmail {
    constructor() {
        this.host = 'smtp.gmail.com';
        this.port = 587;
        this.user = process.env.GMAIL_SMTP_USER;
        this.password = process.env.GMAIL_SMTP_PASSWORD;
        this.from = `Billetera digital <info@test.com>`;
        this.subject = 'Billetera digital';
        this.readHTMLFile = (path, callback) => {
            fs.readFile(path, { encoding: 'utf-8' }, function (err, html) {
                if (err) {
                    throw err;
                    callback(err);
                }
                else {
                    callback(null, html);
                }
            });
        };
        this.init();
        console.log('iniciado');
    }
    init() {
        this.transporter = nodemailer.createTransport(smtpTransport({
            host: this.host,
            port: this.port,
            secure: true,
            auth: {
                user: this.user,
                pass: this.password
            }
        }));
    }
    confirmarCompra(email, data) {
        let that = this;
        const templatesPath = path.resolve(__dirname, `../templates/confirmar_compra.html`);
        this.readHTMLFile(templatesPath, function (err, html) {
            let template = handlebars.compile(html);
            let htmlToSend = template(data);
            const mailOptions = {
                from: that.from,
                to: email,
                subject: that.subject,
                html: htmlToSend
            };
            that.sendEmail(mailOptions);
        });
    }
    sendEmail(mailOptions) {
        let response = false;
        this.transporter.sendMail(mailOptions, function (err, info) {
            if (err) {
                console.log(err);
            }
            else {
                console.log(info);
            }
        });
    }
}
exports.default = ConfigEmail;
