"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jwt = require('jsonwebtoken');
const moment_1 = __importDefault(require("moment"));
/* VERIFICAR TOKEN */
let verificarToken = (req, res, next) => {
    if (!req.headers.authorization) {
        return res.status(403).json({
            ok: false,
            message: 'Tu petición no tiene cabecera de autorización'
        });
    }
    const seed = 'wallet';
    let token = req.get('authorization');
    token = token.replace("Bearer ", "");
    jwt.verify(token, seed, (err, decoded) => {
        if (err) {
            return res.status(401).json({
                ok: false,
                message: 'El token es invalido'
            });
        }
        if (decoded.exp <= moment_1.default().unix()) {
            return res.status(401).json({
                ok: false,
                message: 'El token ha expirado'
            });
        }
        ;
        req.cliente = decoded.data;
        next();
    });
};
module.exports = {
    verificarToken
};
