const jwt = require('jsonwebtoken');
import moment from 'moment';

/* VERIFICAR TOKEN */

let verificarToken = ( req: any, res: any, next: any ) => {

    if(!req.headers.authorization) {
        return res.status(403).json({
            ok: false,
            message: 'Tu petición no tiene cabecera de autorización'
        });
    }

	const seed = 'wallet';

    let token = req.get('authorization');
    
    token = token.replace("Bearer ", "");

	jwt.verify( token, seed, (err: any, decoded: any) => {

		if(err){
			return res.status(401).json({
				ok: false,
				message: 'El token es invalido'
			});	
		}
    
        if(decoded.exp <= moment().unix()) {
            return res.status(401).json({
                ok: false,
                message: 'El token ha expirado'
            });
        };

		req.cliente = decoded.data;
		next();

	});

}

module.exports = {
	verificarToken
};