import { Router, Request, Response } from 'express';
import moment from 'moment';
const router = Router();
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const Cliente = require('../models/cliente_model');
const Billetera = require('../models/billetera_model');

router.post('/api/cliente', async ( req: any, res: Response ) => {

    let body = req.body;
    const saltRounds = 10;

    let clienteDB: any = null;
    
    try{

        body.clave = await  bcrypt.hash(body.clave, saltRounds);

        let cliente_model = new Cliente(body);

        clienteDB = await cliente_model.save();

        /* SE CREA BILLETERA CON SALDO 0 */
        let newBilletera: any = {
            cliente: clienteDB._id,
            saldo: 0			
        };
        await new Billetera(newBilletera).save();
        /* FIN */

        res.status(200).json({
            ok: true,
            title: 'notificación',
            message: 'operación realizada con éxito - cliente registrado',
            type: 'success'
        });	

    } catch( err ){

        return res.status(500).json({
            ok: false,
            title: 'notificación',
            message: err.message,
            type: 'danger'
        });
    }

});

router.post('/api/iniciar-sesion', async ( req: any, res: Response ) => {

	let body = req.body;
	const email = body.email.toLowerCase();
    const clave = body.clave;
    
    let data: any = null;

	try{

		data = await Cliente.findOne({ email: email }).exec();

		if( data == null ){
				return res.status(400).json({
					ok: false,
					title: 'notificación',
					message: 'Error: datos incorrectos',
					type: 'danger'
				});	
			}


		let access: boolean = await bcrypt.compare(clave, data.clave);
		if( !access ){
                return res.status(400).json({
                    ok: false,
                    title: 'notificación',
                    message: 'Error: datos incorrectos',
                    type: 'danger'
                });	
        }
        
        const payload = {
            sub: data._id,
            iat: moment().unix(),
            exp: moment().add(60, "minutes").unix(),
        };

        let token = jwt.sign(payload, 'wallet', { algorithm: 'HS512' });

        let cliente: any = {    documento: data.documento,
                                nombres: data.nombres,
                                email: data.email,
                                celular: data.celular };

        return res.status(200).json({
            ok: true,
            title: 'operación realizada con éxito - sesión iniciada',
            token: `Bearer ${token}`,
            cliente: cliente,
            type: 'success'
        });

	} catch( err ){

		return res.status(500).json({
            ok: false,
            title: 'notificación',
            message: err.message,
            type: 'danger'
        });	
	}
	
});

export default router;