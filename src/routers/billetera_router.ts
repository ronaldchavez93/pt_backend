import { Router, Request, Response } from 'express';
const router = Router();
const randtoken = require('rand-token');
const { verificarToken } = require('../middlewares/autentication');
const Cliente = require('../models/cliente_model');
const Billetera = require('../models/billetera_model');
const Compra = require('../models/compra_model');
import ConfigEmail from '../config/config_email';
const configEmail = new ConfigEmail();

router.get('/api/billetera/:documento/:celular', verificarToken, async ( req: any, res: Response ) => {

    let params =  req.params;
    let clienteDB: any = null;
    let billeteraDB: any = null;

	try{

        clienteDB = await Cliente.findOne({ documento: params.documento, celular: params.celular }).exec();

        if(clienteDB == null){
            return res.status(400).json({
                ok: false,
                title: 'notificación',
                message: 'Error: cliente no existe',
                type: 'danger'
            });
        }
        
        billeteraDB = await Billetera.findOne({ cliente: clienteDB._id }).exec();

        return res.status(200).json({
            ok: true,
            title: 'notificación',
            message: 'operación realizada con éxito',
            billetera: billeteraDB.saldo,
            type: 'success'
        });

	} catch( err ){

		return res.status(500).json({
            ok: false,
            title: 'notificación',
            message: err.message,
            type: 'danger'
        });
	}
	
});

router.put('/api/billetera/recargar', verificarToken, async ( req: any, res: Response ) => {

    let body = req.body;
    let clienteDB: any = null;
    let billeteraDB: any = null;

	try{

        clienteDB = await Cliente.findOne({ documento: body.documento, celular: body.celular }).exec();

        if(clienteDB == null){
            return res.status(400).json({
                ok: false,
                title: 'notificación',
                message: 'Error: cliente no existe',
                type: 'danger'
            });
        }

        billeteraDB = await Billetera.findOne({ cliente: clienteDB._id }, 'saldo').exec();
        let newSaldo: number = parseFloat(billeteraDB.saldo) + parseFloat(body.saldo);
        await Billetera.updateOne( { cliente: clienteDB._id }, { $set: { saldo: newSaldo } });

        return res.status(200).json({
            ok: true,
            title: 'notificación',
            message: 'operación realizada con éxito',
            type: 'success'
        });

	} catch( err ){

		return res.status(500).json({
            ok: false,
            title: 'notificación',
            message: err.message,
            type: 'danger'
        });
	}
	
});

router.post('/api/billetera/pagar', verificarToken, async ( req: any, res: Response ) => {

    let body = req.body;
    let clienteDB: any = null;
    let billeteraDB: any = null;
    let compraDB: any = null;

	try{

        let token = randtoken.generate(6);

        clienteDB = await Cliente.findOne({ documento: body.documento, celular: body.celular }).exec();

        if(clienteDB == null){
            return res.status(400).json({
                ok: false,
                title: 'notificación',
                message: 'Error: cliente no existe',
                type: 'danger'
            });
        }

        billeteraDB = await Billetera.findOne( { cliente: clienteDB._id }).exec();

        if(billeteraDB == null){
            return res.status(400).json({
                ok: false,
                title: 'notificación',
                message: 'Error: billetera no existe',
                type: 'danger'
            });
        }

        if(parseFloat(body.monto) > parseFloat(billeteraDB.saldo)){
            return res.status(400).json({
                ok: false,
                title: 'notificación',
                message: 'Error: El monto de la compra es mayor al saldo disponible en la billetera, por favor recarge',
                type: 'danger'
            });
        }

        /* SE REGISTRA LA COMPRA */
        let newCompra: any = {
            cliente: clienteDB._id,
            token: token,
            monto: body.monto			
        };
        compraDB = await new Compra(newCompra).save();
        /* FIN */

        /* SE ENVIA EMAIL CON TOKEN PARA CONFIRMAR LA COMPRA */
        let data = { 
            token: token 
        }; 

        configEmail.confirmarCompra(clienteDB.email,data);
        /* FIN */

        return res.status(200).json({
            ok: true,
            title: 'notificación',
            message: 'operación realizada con éxito - se ha enviado un email con el token de confirmación',
            compra: compraDB,
            type: 'success'
        });

	} catch( err ){

		return res.status(500).json({
            ok: false,
            title: 'notificación',
            message: err.message,
            type: 'danger'
        });
	}
	
});

router.put('/api/billetera/confirmar-pago', verificarToken, async ( req: any, res: Response ) => {

    let body = req.body;
    let clienteDB: any = null;
    let billeteraDB: any = null;
    let compraDB: any = null;

	try{

        clienteDB = await Cliente.findOne({ documento: body.documento, celular: body.celular }).exec();

        if(clienteDB == null){
            return res.status(400).json({
                ok: false,
                title: 'notificación',
                message: 'Error: cliente no existe',
                type: 'danger'
            });
        }

        billeteraDB = await Billetera.findOne( { cliente: clienteDB._id }).exec();

        if(billeteraDB == null){
            return res.status(400).json({
                ok: false,
                title: 'notificación',
                message: 'Error: billetera no existe',
                type: 'danger'
            });
        }

        compraDB = await Compra.findOne( { _id: body.compra, token: body.token }).exec();

        if(compraDB == null){
            return res.status(400).json({
                ok: false,
                title: 'notificación',
                message: 'Error: token de confirmación de compra invalido',
                type: 'danger'
            });
        }

        if(parseFloat(compraDB.monto) > parseFloat(billeteraDB.saldo)){
            /* SE ACTUALIZA EL ESTATUS DE LA COMPRA */
            await Compra.updateOne( { _id: body.compra }, { $set: { estatus: 'RECHAZADO' } });
            /* FIN */
            return res.status(400).json({
                ok: false,
                title: 'notificación',
                message: 'Error: El monto de la compra es mayor al saldo disponible en la billetera, por favor recarge',
                type: 'danger'
            });
        }

        /* SE DESCUENTA EL MONTO DE LA COMPRA EN LA BILLETERA */
        let newSaldo: number = parseFloat(billeteraDB.saldo) - parseFloat(compraDB.monto);
        await Billetera.updateOne( { cliente: clienteDB._id }, { $set: { saldo: newSaldo } });
        /* FIN */

        /* SE ACTUALIZA EL ESTATUS DE LA COMPRA */
        await Compra.updateOne( { _id: body.compra }, { $set: { estatus: 'CONFIMADO' } });
        /* FIN */

        return res.status(200).json({
            ok: true,
            title: 'notificación',
            message: 'operación realizada con éxito',
            saldo: newSaldo,
            type: 'success'
        });

	} catch( err ){

		return res.status(500).json({
            ok: false,
            title: 'notificación',
            message: err.message,
            type: 'danger'
        });
	}
	
});

export default router;