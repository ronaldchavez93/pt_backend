import nodemailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');
const handlebars = require('handlebars'); 
const fs = require('fs'); 
import path = require('path');

export default class ConfigEmail {

	private host: string = 'smtp.gmail.com';
	private port: number = 587;
	private user: any = process.env.GMAIL_SMTP_USER;
	private password: any = process.env.GMAIL_SMTP_PASSWORD;
	private transporter: any;
	private from: string = `Billetera digital <info@test.com>`;
	private subject: string = 'Billetera digital';

	constructor(){

		this.init();
		console.log('iniciado');

	}

	private init(){
		
		this.transporter = nodemailer.createTransport(smtpTransport({
					        host: this.host,
					        port: this.port,
					        secure: true,
					        auth: {
					            user: this.user,
					            pass: this.password
					        }
    	}));

	}

	confirmarCompra(email: string, data: any){

		let that = this;


		const templatesPath = path.resolve(__dirname, `../templates/confirmar_compra.html`);

		this.readHTMLFile(templatesPath, function(err: any, html: any) { 

		    let template = handlebars.compile(html); 

		    let htmlToSend = template(data); 

		    const mailOptions = { 
		     from: that.from, 
		     to : email, 
		     subject : that.subject, 
		     html : htmlToSend 
		    }; 

            that.sendEmail(mailOptions);

		}); 
	}

	readHTMLFile = (path: any, callback: Function) => { 
	    fs.readFile(path, {encoding: 'utf-8'}, function (err: any, html: any) { 
	     if (err) { 
	      throw err; 
	      callback(err); 
	     } 
	     else { 
	      callback(null, html); 
	     } 
	    }); 
	};

	private sendEmail( mailOptions: any ){

		let response: boolean = false;

		this.transporter.sendMail(mailOptions, function (err: any, info: any) {
			 if (err){
			 	console.log(err);
			 }else{
			 	console.log(info);
			 }
		 });

		}	

}