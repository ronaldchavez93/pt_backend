import { Document, Model, model, Schema } from 'mongoose';

let BilleteraSchema: Schema;

BilleteraSchema = new Schema({

	saldo: {
    type: Number,
    required: [true, 'saldo es requerido'],
		default: 0
  },
  cliente: {
    type: Schema.Types.ObjectId, 
		ref: 'clientes' 
  }

});

interface IBilletera extends Document {
    saldo: Number;
    cliente: Schema.Types.ObjectId;
  }
  
module.exports = model<IBilletera>('billeteras', BilleteraSchema, 'billeteras');