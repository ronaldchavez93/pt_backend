import { Document, Model, model, Schema } from 'mongoose';

let CompraSchema: Schema;

let validStatus = {
	values: ['PENDIENTE', 'CONFIMADO', 'RECHAZADO'],
	message: '{VALUE} no es un estatus valido'
};

CompraSchema = new Schema({

    cliente: {
        type: Schema.Types.ObjectId, 
	    ref: 'clientes' 
    },
    token: {
        type: String,
        required:  [true, 'Token requerido'],
		trim: true
    },
    monto: {
        type: Number,
        required: [true, 'monto es requerido'],
		default: 0
    },
    estatus: {
        type: String,
        trim: true,
        default: 'PENDIENTE',
        enum: validStatus
    },
    fecha: {
        type: Date,
        default: Date.now
    },

});

interface ICompra extends Document {
    cliente: Schema.Types.ObjectId;
    token: String;
    monto: Number;
    estatus: String;
    fecha: Date;
}
  
module.exports = model<ICompra>('compras', CompraSchema, 'compras');