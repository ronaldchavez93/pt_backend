import express = require('express');
import mongoose from 'mongoose';
import path = require('path');
import * as bodyParser from "body-parser";
const { mongoDb } = require('./config/config');

/* ROUTERS */
import clienteRouter from '../routers/cliente_router';
import billeteraRouter from '../routers/billetera_router';
/* FIN */

/* SWAGGER - DOCUMENTACIÓN DE LAS API */
import swaggerUi from 'swagger-ui-express';
import * as swaggerDocument from './swagger.json';
/* FIN */


import cors = require('cors');

export default class Server {

	public app: express.Application;
	public port: number;

	constructor(port: number){
		this.port = port;
		this.app = express();
		this.config();
        this.setupDb();
		this.app.use(clienteRouter);
		this.app.use(billeteraRouter);
	}

	static init(port: number){

		return new Server( port );

	}

	private setupDb(): void {
	    mongoose.connect(mongoDb, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true }, (err: any) => {
	    if(err) throw err;

		console.log('base de datos ONLINE');

	    });
  	}

  	private config(): void{
        
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
		this.app.use(cors(this.configCors()));
		this.app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
    }

    private configCors(){

		let originsWhitelist = [
		  'http://localhost:3000'
		];

		let corsOptions = {
		  origin: function(origin: any, callback: any){
		        let isWhitelisted = originsWhitelist.indexOf(origin) !== -1;
		        callback(null, isWhitelisted);
		  },
		  credentials:true
		}
		return corsOptions;

    }

	start(callback: () => void) : void {
		const server = this.app.listen( this.port, callback);
	}

}