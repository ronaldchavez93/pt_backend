import Server from './server/server';
const { port } = require('./server/config/config');

const server = Server.init( port );

server.start(() => {
    console.log(`servidor corriendo en el puerto ${port}`);
});